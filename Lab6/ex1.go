package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

func NewClient(name string, surname string, acc_num string, deposit float64, credits float64) Client {
	return Client{name, surname, acc_num, deposit, credits, nil}
}
func (this *Client) SetName(n string) {
	this.Name = n
}
func (this *Client) SetSurName(n string) {
	this.Surname = n
}
func (this *Client) SetDeposit(d float64) {
	this.c_Deposit = d
}
func (this *Client) _AddToDeposit(d float64) {
	this.c_Deposit += d
	this.Bank._AddToDeposit(d)
}
func (this *Client) _SubFromDeposit(d float64) {
	this.c_Deposit -= d
	this.Bank._SubFromDeposit(d)
}
func (this *Client) SetCredit(c float64) {
	this.c_Credit = c
}

// credit is negative
func (this *Client) _AddToCredit(c float64) {
	this.c_Credit -= c
	this.Bank._AddToCredit(c)
}
func (this *Client) _SubFromCredit(c float64) {
	this.c_Credit += c
	this.Bank._SubFromCredit(c)
}
func (this Client) GetName() string {
	return this.Name
}
func (this Client) GetSurName() string {
	return this.Surname
}
func (this Client) GetFullName() string {
	return this.Name + " " + this.Surname
}
func (this Client) GetDeposit() float64 {
	return this.c_Deposit
}
func (this Client) GetCredit() float64 {
	return this.c_Credit
}
func (this Client) PrintClientInfo() {
	fmt.Printf("%-10s %-10.2f %-10.2f %-10s\n", this.GetFullName(), this.c_Deposit, this.c_Credit, this.Account_Number)
}
func (this *Client) AddToDeposit(money float64) {
	this._AddToDeposit(money)
}
func (this *Client) TakeFromDeposit(money float64) {
	if !this.CheckMoney(money) {
		return
	}
	this._SubFromDeposit(money)
}
func (this *Client) PayCredit(money float64) {
	if money < 0 {
		//fmt.Printf("%s, you cannot pay less than 0 for credit.\n", this.GetFullName())
		return
	}
	if !this.CheckMoney(money) {
		return
	}

	if money > -this.GetCredit() {
		extra := money + this.GetCredit()
		money -= extra
	}
	this._SubFromDeposit(money)
	this._SubFromCredit(money)

}
func (this *Client) TakeCredit(money float64) {
	if !this.Bank.CheckMoney(money) {
		return
	}
	this._AddToDeposit(money)
	this._AddToCredit(money)

}

func (this *Client) CheckMoney(m float64) bool {
	if this.GetDeposit() < m {
		fmt.Printf("%s, you do not have so much money: %.3f\n", this.GetFullName(), m)
		fmt.Printf("You have: %.3f\n", this.GetDeposit())
		return false
	}
	return true
}

type Client struct {
	Name           string
	Surname        string
	Account_Number string
	c_Deposit      float64 // сума на депозиті клієнта
	c_Credit       float64 // сума кредитів
	Bank           *Bank   // знадобиться
}

func NewBank(name string, bank_money float64) Bank {
	return Bank{name, bank_money, 0, 0, []*Client{}}
}
func (this *Bank) SetName(n string) {
	this.Name = n
}
func (this *Bank) SetBankMoney(m float64) {
	this.Bank_Money = m
}
func (this *Bank) SetDeposit(d float64) {
	this.Deposits = d
}
func (this *Bank) SetCredit(c float64) {
	this.Credits = c
}
func (this Bank) GetDeposit() float64 {
	return this.Deposits
}
func (this Bank) GetBankMoney() float64 {
	return this.Bank_Money
}
func (this Bank) GetClient(index int) *Client {
	return this.Clients[index]
}
func (this *Bank) GetClientByAccountNumber(acc_num string) {
	for _, c := range this.Clients {
		if c.Account_Number == acc_num {
			c.PrintClientInfo()
		}
	}
}
func (this *Bank) GetClientBySurname(surname string) {
	for _, c := range this.Clients {
		if c.Surname == surname {
			c.PrintClientInfo()
		}
	}
}

func (this *Bank) AddClient(c *Client) {
	this.Clients = append(this.Clients, c)
	this._AddToDeposit(c.c_Deposit)
	this._AddToCredit(-c.c_Credit)
	c.Bank = this
}
func (this *Bank) RemoveClient(index int) {
	this.Clients = append(this.Clients[:index], this.Clients[index+1:]...)
}
func (this *Bank) PrintClients() {
	fmt.Printf("%-10s %-10s %-10s %-10s\n", "Bank", "Deposits", "Credits", "Bank Money")
	fmt.Printf("%-10s %-10.2f %-10.2f %-10.2f\n", this.Name, this.Deposits, this.Credits, this.Bank_Money)
	fmt.Printf("%-30s %s\n", "Client", "Account Number")
	for _, c := range this.Clients {
		c.PrintClientInfo()
	}
}
func (this *Bank) _AddToDeposit(m float64) {
	this.Deposits = this.Deposits + m
}
func (this *Bank) _SubFromDeposit(m float64) {
	this.Deposits -= m
}
func (this *Bank) _AddToCredit(m float64) {
	this.Credits += m
	this._TakeMoney(m)
}
func (this *Bank) _SubFromCredit(m float64) {
	this.Credits -= m
	this._AddMoney(m)
}
func (this *Bank) _AddMoney(m float64) {
	this.Bank_Money += m
}
func (this *Bank) _TakeMoney(m float64) {
	this.Bank_Money -= m
}

func (this *Bank) CheckMoney(m float64) bool {
	if this.GetBankMoney() < m {
		/*fmt.Printf("%s bank does not have so much money: %.3f\n", this.Name, m)
		fmt.Printf("Bank has: %.3f\n", this.Bank_Money)*/
		return false
	}
	return true
}

type Bank struct {
	Name       string
	Bank_Money float64
	Deposits   float64
	Credits    float64
	Clients    []*Client
}

func main() {
	is_bank_created := false
	bank := Bank{}
	ch := make(chan struct{}, 1)
	go func() {
		ch <- struct{}{}
	}()

	for true {
		var choice int
		var tmp string
		var err error
		fmt.Println("1: Create bank\n2: Add client for credits\n3: Add client for deposits")
		fmt.Println("4: Output client info by his account number")
		fmt.Println("5: Output client info by his surname")
		fmt.Println("6: Output all clients info")
		fmt.Println("0: Exit")
		for true {
			fmt.Print("-> ")
			_, _ = fmt.Scanln(&tmp)
			choice, err = strconv.Atoi(tmp)
			if err != nil || choice < 0 || choice > 6 {
				PrintInRed("Wrong input.")
			} else {
				break
			}
		}
		switch choice {
		case 0:
			return
		case 1:
			{
				if is_bank_created {
					fmt.Printf("Bank is already created and its name: %s\n", bank.Name)
					break
				}
				var (
					bank_name  string
					bank_money float64
				)

				fmt.Print("Enter bank name: ")
				_, _ = fmt.Scanln(&bank_name)
				for bank_name == "" {
					PrintInRed("Enter bank name again: ")
					_, _ = fmt.Scanln(&bank_name)
				}
				for true {
					fmt.Print("Enter bank money amount:  ")
					_, _ = fmt.Scanln(&tmp)
					bank_money, err = strconv.ParseFloat(tmp, 64)
					if err != nil || bank_money <= 0 {
						PrintInRed("Enter bank money amount again.")
					} else {
						break
					}
				}
				bank = NewBank(bank_name, bank_money)
				is_bank_created = true
			}
		case 2, 3:
			{
				if !is_bank_created {
					fmt.Println("Bank is not created.")
					break
				}

				var (
					name    string
					surname string
					acc_num string
				)
				fmt.Print("Enter client name: ")
				_, _ = fmt.Scanln(&name)
				for name == "" {
					PrintInRed("Enter client name again: ")
					_, _ = fmt.Scanln(&name)
				}
				fmt.Print("Enter client surname: ")
				_, _ = fmt.Scanln(&surname)
				for surname == "" {
					PrintInRed("Enter client surname again: ")
					_, _ = fmt.Scanln(&surname)
				}
				fmt.Print("Enter client account number: ")
				_, _ = fmt.Scanln(&acc_num)
				for acc_num == "" {
					PrintInRed("Enter client account number again: ")
					_, _ = fmt.Scanln(&acc_num)
				}
				if choice == 2 {
					client := NewClient(name, surname, acc_num, 500, -500)
					go func() {
						<-ch
						bank.AddClient(&client)
						ch <- struct{}{}
						go ClientCreditWork(&client, ch)
					}()
				} else {
					client := NewClient(name, surname, acc_num, 500, -10)
					go func() {
						<-ch
						bank.AddClient(&client)
						ch <- struct{}{}
						go ClientDepositWork(&client, ch)
					}()
				}

			}
		case 4:
			{
				if !is_bank_created {
					fmt.Println("Bank is not created.")
					break
				}
				acc_num := ""
				fmt.Print("Enter account number: ")
				fmt.Scanln(&acc_num)

				go func() {
					<-ch
					bank.GetClientByAccountNumber(acc_num)
					ch <- struct{}{}
				}()

			}
		case 5:
			{
				if !is_bank_created {
					fmt.Println("Bank is not created.")
					break
				}
				surname := ""
				fmt.Print("Enter client surname: ")
				fmt.Scanln(&surname)

				go func() {
					<-ch
					bank.GetClientBySurname(surname)
					ch <- struct{}{}
				}()
			}
		case 6:
			{
				if !is_bank_created {
					fmt.Println("Bank is not created.")
					break
				}
				go func() {
					<-ch
					bank.PrintClients()
					ch <- struct{}{}
				}()

			}

		}
		fmt.Println("Press enter...")
		_, _ = fmt.Scanln()
	}

}

func PrintInRed(text string) {
	fmt.Println("\033[31m" + text + "\033[0m")
}
func PrintInGreen(text string) {
	fmt.Println("\033[32m" + text + "\033[0m")
}

func ClientDepositWork(c *Client, ch chan struct{}) {
	for c.GetDeposit() > 0 {

		choice := rand.Intn(2) + 1
		<-ch
		switch choice {
		case 1:
			{
				c.AddToDeposit(100)
				break
			}
		case 2:
			{
				c.TakeFromDeposit(100)
				break
			}
		}
		ch <- struct{}{}
		time.Sleep(time.Second)
	}
	PrintInGreen(c.GetFullName() + " done working with deposit\n")
}

func ClientCreditWork(c *Client, ch chan struct{}) {
	for c.GetCredit() < 0 {

		choice := rand.Intn(2) + 1
		<-ch
		switch choice {
		case 1:
			{
				c.TakeCredit(100)
				break
			}
		case 2:
			{
				c.PayCredit(100)
				break
			}
		}
		ch <- struct{}{}
		time.Sleep(time.Second)
	}
	PrintInGreen(c.GetFullName() + " done working with credits\n")
}

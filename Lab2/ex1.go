package main

import "fmt"

func main() {
	arr := []int{2, 2, 12, 14, 10, 5, 3}

	min := arr[0]

	for _, val := range arr {
		if min > val {
			min = val
		}
	}

	fmt.Println(min)

	total := 0
	for _, v := range arr {
		total += v
	}
	fmt.Println(total / int(len(arr)))

}
